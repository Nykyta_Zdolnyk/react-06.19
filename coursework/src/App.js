import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Link } from 'react-router-dom';

import store from './redux/store';

import './App.css';
import Popular from './components/Popular';
import Find from './components/Find';
import Poster from './components/Poster';
import Genres from './components/Genres';
import Navigation from './components/nav';
import Search from './components/Search';
import Catalogs from './components/Catalogs'; 
import Main from './components/Main';


const Root = () => {
	return (
		<>
			<Provider store={store}>
				<BrowserRouter>
					<Navigation />
					<Find />

					{/* <Route exact path="/" component={Main}/> */}
					<Route exact path="/popular" component={Popular}/>
					<Route exact path="/movies/:filmid" component={Poster}/>
					<Route exact path="/discover/movie/genre/:genreid" component={Genres}/>
					<Route exact path="/search/movie" component={Search}/>
					<Route exact path="/catalogs" component={Catalogs}/>
				</BrowserRouter>
			</Provider>
		</>
	);
}

export default Root;

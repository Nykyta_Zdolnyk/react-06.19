import React from 'react';
import { connect } from 'react-redux';

import ContentItem from './contentItem';
import Content from './Content';

import { getData } from '../actions';


class Main extends React.Component{


    componentDidMount() {
        this.props.getData()
    }

    render(){
       const { films, loaded } = this.props;

        return(

            <>
                <h1>Popular Movies</h1>
                <Content>
                        {
                            loaded ? 
                            (
                                films.results.map( film => <ContentItem key={film.id} param={film}/> )
                            ) : 
                            (
                                "Loading ..."
                            )
                        }
                </Content>


                
            </>
           
        )
    }
}

/*
    Redux
*/

const mapStateToProps = ( state, ownProps ) => ({
    films: state.popularFilms.data,
    loaded: state.popularFilms.loaded
})

const mapDispatchToProps = ( dispatch ) => ({

    getData: () => dispatch( getData() )
    
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);


import React, { Component } from 'react';
import { connect} from 'react-redux';
import moment from 'moment'

import LoaderImage from './LoaderImage';
import CustomLink from './CustomLink';
import Text from './Text';

import { getData } from '../actions/singleFilm';

moment.locale('ru');

class Poster extends Component {


    componentDidMount() {

        this.props.getData()
    }

    render() {
        const { 
            poster_path,
            title, 
            release_date, 
            overview, 
            budget,
            genres
        } = this.props.data;

        const { loaded } = this.props
        
        

        return(
            <>
               {
                   loaded ? 
                   (
                        <div  className='film-item'>
                            <LoaderImage src={`https://image.tmdb.org/t/p/w600_and_h900_bestv2${poster_path}`}  />
                            <div className='info'>
                                    <h2>{title}</h2>
                                    <Text title='Дата выхода:'>
                                        {moment(release_date).format('LL')}
                                    </Text>
                                    <Text title='Обзор:'>
                                        {overview}
                                    </Text>
                                    <Text title='Бюджет:'>
                                        {budget}$
                                    </Text>

                                <Text title='Жанры:'>
                                    <ul>
                                        {
                                            genres.map(item => (
                                                <li key={item.id}>
                                                    <CustomLink url={`/discover/movie/genre/${item.id}`}>
                                                            {item.name}
                                                    </CustomLink>
                                                </li>
                                            ))
                                        }
                                    </ul>
                                </Text>
                            </div>  
                        </div>
                   ) : 
                   (
                       'Loading ...'
                    )
               }
            </>
        )
    }
}

/*
    Redux
*/

const mapStateToProps = ( state, ownProps ) => ({
    data: state.singleFilm.data,
    loaded: state.singleFilm.loaded
})

const mapDispatchToProps = ( dispatch, ownProps ) => {

    const { filmid } = ownProps.match.params;
    return { 
        getData: () => dispatch( getData(filmid) )
    }
    
};

export default connect(mapStateToProps, mapDispatchToProps)(Poster);


import React, { Component } from 'react';

import { connect } from 'react-redux';
import { getData } from '../actions/catalogs';

import CustomLink from './CustomLink';

class Catalogs extends Component {


componentDidMount() {

    this.props.getData();
}

    render() {

        const { loaded, catalog } = this.props;
        console.log(catalog);

        return(

            <div>
                {
                    loaded ? (
                        <ul>
                        <h2>Список жанров</h2>

                                        {
                                            catalog.genres.map(item => (
                                                <li key={item.id}>
                                                    <CustomLink url={`/discover/movie/genre/${item.id}`}>
                                                            {item.name}
                                                    </CustomLink>
                                                </li>
                                            ))
                                        }
                                    </ul>
                    ) : 
                    (
                        'Loadnig...'
                    )
                }
            </div>
    
        )
    }
    
    
}


/*
    Redux
*/

const mapStateToProps = ( state, ownProps ) => ({

    catalog: state.catalog.data,
    loaded: state.catalog.loaded
})

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    
    getData: () => (dispatch( getData()) )
});

export default connect(mapStateToProps, mapDispatchToProps)(Catalogs)
import React from 'react';
import { connect } from 'react-redux';

import ContentItem from './contentItem';
import Content from './Content';



const Search = ({ films, loaded }) => {
    console.log('props, ', films, loaded);

    return(
        <Content>
            {
                loaded ? 
                    (
                        films.results.map( film => <ContentItem key={film.id} param={film}/> )
                    ) : 
                    (
                        "Loading ..."
                    )
            }
        </Content>
    )
}

/*
    Redux
*/

const mapStateToProps = ( state, ownProps ) => ({

    films: state.find.data,
    loaded: state.find.loaded
    
})

const mapDispatchToProps = ( dispatch, ownProps ) => ({
   
});

export default connect(mapStateToProps, null)(Search);
import React from 'react';

import  LoaderImage from './LoaderImage';
import CustomLink from './CustomLink';
import Text from './Text'

const ContentItem = ({ param }) => {

    const { title, poster_path, release_date, overview, id } = param;
    return(
        <div className='content-item'>
                <CustomLink url={`/movies/${id}`}>
                        <LoaderImage 
                                    src={`https://image.tmdb.org/t/p/w600_and_h900_bestv2${poster_path}`}        
                        />
                    </CustomLink>
                   
            <div className='movie-info'>
                <CustomLink url={`/movies/${id}`}>
                    <h4> {title} </h4>
                </CustomLink>
            </div>           
        </div>
    )
}

export default ContentItem;
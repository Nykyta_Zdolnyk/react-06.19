import React from 'react';

const CustomLink = ({url, children}) => {

    return (
        <div className='custom-link'>
            <a href={url}>
                {
                    children
                }
            </a>
        </div>
    )
}

export default CustomLink;
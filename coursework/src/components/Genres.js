import React, { Component } from 'react';
import { connect } from 'react-redux';

import Content from './Content';
import ContentItem from './contentItem';

import { getData } from '../actions/genres'


class Genres extends Component {

    componentDidMount() {

        this.props.getData();
    }

    render() {
        
        const { loaded, films } = this.props;
        console.log('films', films)
        return(
            <Content>
                        {
                            loaded ? 
                            (
                                films.results.map( film => <ContentItem key={film.id} param={film}/> )
                            ) : 
                            (
                                "Loading ..."
                            )
                        }
            </Content>
        )
    }
}
/*
    Redux
*/

const mapStateToProps = ( state, ownProps ) => ({

    films: state.genres.data,
    loaded: state.genres.loaded

})

const mapDispatchToProps = ( dispatch, ownProps ) => {
    const { genreid } = ownProps.match.params;

    return {
        getData: () => dispatch( getData(genreid) )
    }
    
};

export default connect(mapStateToProps, mapDispatchToProps)(Genres);
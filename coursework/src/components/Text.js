import React from 'react';

const Text = ({title, children}) => {

    return(
        <div >
            {
                title ? (<div> <b>{title} </b> </div>) : (null)
            }
            
            <div> {children} </div>
        </div>
    )
}

export default Text;
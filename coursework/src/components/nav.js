import React from 'react';
import { NavLink } from 'react-router-dom';


const style = {
    color: 'green',
    margin: "20px"
}

const Navigation = () => {

    return (
        <div>
            <NavLink exact to='/' activeClassName='active' style={style}> Главная </NavLink>
            <NavLink exact to='/popular' activeClassName='active' style={style}> Популярные </NavLink>
            <NavLink exact to='/catalogs' activeClassName='active' style={style}> Каталоги </NavLink>
        </div>
    )
}

export default Navigation;

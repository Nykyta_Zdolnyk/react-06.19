import React, { Component } from "react";
import { Link } from "react-dom";

class LoaderImg extends Component {
  state = {
    loaded: false
  };

  componentDidMount() {
    const { src } = this.props;

    let img = document.createElement("img");
    img.onload = this.onload;
    img.src = src;
  }

  onload = () => {
    this.setState({
      loaded: true
    });
  };

  render() {
    const { loaded } = this.state;
    const { src } = this.props;

    return (
      <>
          {
              loaded ? 
                <img className="img" src={src} /> : 
                "Loading"
          }
        
      </>
    );
  }
}

export default LoaderImg;

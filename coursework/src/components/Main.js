import React, { Component } from 'react';

import { connect } from 'react-redux';
import { getData } from '../actions/latest';

import Content from './Content';
import ContentItem from './contentItem';


class Main extends Component {

    componentDidMount() {

        this.props.getData()
    }

    render() {

        const { data, loaded } = this.props;
        console.log('data', data)

        return(
            <>
               <h1>Popular Movies</h1>
                <Content>
                        {
                            loaded ? 
                            (
                                 <ContentItem key={data.id} param={data}/>
                            ) : 
                            (
                                "Loading ..."
                            )
                        }
                </Content>
            </>
        )
    }
}

/*
    Redux
*/

const mapStateToProps = ( state, ownProps ) => ({

    data: state.latest.data,
    loaded: state.latest.loaded

})

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    getData: () => dispatch( getData() ) 
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
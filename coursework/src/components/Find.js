import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { getData } from '../actions/find';


class Find extends Component {

    state = {
        value: ''
    }

    changeHandler = (e) => {
        this.setState({
            value: e.target.value
        })
    }

    clickChandler = () => {
        const { value } = this.state;

        this.props.search(value);
    }

    render() {

        const { changeHandler, clickChandler } = this;
        const { value } = this.state;

        return(
            <div className='form'>
                <input type='text' onChange={changeHandler} value={value} placeholder='Поиск фильма'/>
                <Link to='/search/movie'>
                    <button onClick={clickChandler}> Поиск </button>
                </Link>

            </div>
        )
    }
}

/*
    Redux
*/

const mapStateToProps = ( state, ownProps ) => ({


})

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    
    search: (query) => (dispatch( getData(query)) )
});


export default connect(null, mapDispatchToProps)(Find);
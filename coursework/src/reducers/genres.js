import { 
    REQ_GENRES_FILMS,
    RES_GENRES_FILMS,
    ERROR_GENRES_FILMS
} from '../actions';


const genresInitialState = {
    loaded: false,
    loading: false,
    errors: [],
    data: []
};

const genreFilmsReducer = ( state = genresInitialState, action) => {
    switch( action.type ){

        case REQ_GENRES_FILMS:
            return {
                ...state,
                loading: true
            }

        case RES_GENRES_FILMS:
            return {
                ...state,
                loading: false,
                loaded: true,
                data: action.payload
            }

        default:
            return state;
    }
}

export default genreFilmsReducer;
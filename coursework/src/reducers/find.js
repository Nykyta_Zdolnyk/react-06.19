import { 
    REQ_FIND_FILMS,
    RES_FIND_FILMS,
    ERROR_FIND_FILMS 
} from '../actions';


const findInitialState = {
    loaded: false,
    loading: false,
    errors: [],
    data: [],
    query: ''
};

const findReducer = ( state = findInitialState, action) => {
    switch( action.type ){

        case REQ_FIND_FILMS:
            return {
                ...state,
                loading: true
            }

        case RES_FIND_FILMS:
            return {
                ...state,
                loading: false,
                loaded: true,
                data: action.payload
            }

        default:
            return state;
    }
}

export default findReducer;
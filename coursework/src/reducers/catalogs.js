import { 
    REQ_CATALOG_FILMS,
    RES_CATALOG_FILMS,
    ERROR_CATALOG_FILMS
} from '../actions';


const catalogsInitialState = {
    loaded: false,
    loading: false,
    errors: [],
    data: []
};

const catalogsReducer = ( state = false, action) => {
    switch( action.type ){

        case REQ_CATALOG_FILMS:
            return {
                ...state,
                loading: true
            }

        case RES_CATALOG_FILMS:
            return {
                ...state,
                loading: false,
                loaded: true,
                data: action.payload
            }

        default:
            return state;
    }
}

export default catalogsReducer;
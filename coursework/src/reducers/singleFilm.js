import { 
    REQ_SINGLE_FILMS,
    RES_SINGLE_FILMS,
    ERROR_SINGLE_FILMS 
} from '../actions';


const singleFilmInitialState = {
    loaded: false,
    loading: false,
    errors: [],
    data: []
};

const singletFilmReducer = ( state = singleFilmInitialState, action) => {
    switch( action.type ){

        case REQ_SINGLE_FILMS:
            return {
                ...state,
                loading: true
            }

        case RES_SINGLE_FILMS:
            return {
                ...state,
                loading: false,
                loaded: true,
                data: action.payload
            }

        default:
            return state;
    }
}

export default singletFilmReducer;
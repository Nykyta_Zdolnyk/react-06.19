import { 
    REQ_POPULAR_FILMS,
    RES_POPULAR_FILMS,
    ERROR_POPULAR_FILMS 
} from '../actions';


const popularFilmsInitialState = {
    loaded: false,
    loading: false,
    errors: [],
    data: []
};

const populartFilmsReducer = ( state = popularFilmsInitialState, action) => {
    switch( action.type ){

        case REQ_POPULAR_FILMS:
            return {
                ...state,
                loading: true
            }

        case RES_POPULAR_FILMS:
            return {
                ...state,
                loading: false,
                loaded: true,
                data: action.payload
            }

        default:
            return state;
    }
}

export default populartFilmsReducer;
import { combineReducers } from 'redux';

import popularFilms from './popular';
import singleFilm from './singleFilm';
import genres from './genres';
import find from './find';
import catalog from './catalogs';
import latest from './latest';

const reducer = combineReducers({
    popularFilms,
    singleFilm,
    genres,
    find,
    catalog,
    latest
});

export default reducer;

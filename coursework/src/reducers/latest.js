import { 
    REQ_LATEST_FILMS,
    RES_LATEST_FILMS,
    ERROR_LATEST_FILMS
} from '../actions';


const latestFilmsInitialState = {
    loaded: false,
    loading: false,
    errors: [],
    data: []
};

const latestFilmsReducer = ( state = latestFilmsInitialState, action) => {
    switch( action.type ){

        case REQ_LATEST_FILMS:
            return {
                ...state,
                loading: true
            }

        case RES_LATEST_FILMS:
            return {
                ...state,
                loading: false,
                loaded: true,
                data: action.payload
            }

        default:
            return state;
    }
}

export default latestFilmsReducer;
/*
    API Key (v3 auth)
    ba0611d0e350944869912b478c93ea55

    Example API Request
    https://api.themoviedb.org/3/movie/550?api_key=ba0611d0e350944869912b478c93ea55

    API Read Access Token (v4 auth)
    eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJiYTA2MTFkMGUzNTA5NDQ4Njk5MTJiNDc4YzkzZWE1NSIsInN1YiI6IjVkNDI5Y2ZiOTVjMGFmMDAxNGQ4YTNiNyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.LoUw4JZR6Z8pacF7Yc3Bhqy3vJOnu2ihmreojAF0OD8
*/
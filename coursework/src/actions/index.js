export * from './popularFilms'; 
export * from './singleFilm'; 
export * from './genres';
export * from './find';
export * from './catalogs';
export * from './latest';

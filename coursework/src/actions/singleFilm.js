import axios from '../helpers/axios';

export const REQ_SINGLE_FILMS = "REQ_SINGLE_FILMS";
export const RES_SINGLE_FILMS = "RES_SINGLE_FILMS";
export const ERROR_SINGLE_FILMS = "ERROR_SINGLE_FILMS";



export const getData = (filmid) => ( dispatch, getState ) => {

    dispatch({ type: REQ_SINGLE_FILMS });

    axios.get( `/movie/${filmid}`)
        .then( res => dispatch({
                type: RES_SINGLE_FILMS,
                payload: res.data
            }))   
}
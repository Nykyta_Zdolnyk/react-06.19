import axios from '../helpers/axios';

export const REQ_GENRES_FILMS = "REQ_GENRES_FILMS";
export const RES_GENRES_FILMS = "RES_GENRES_FILMS";
export const ERROR_GENRES_FILMS = "ERROR_GENRES_FILMS";



export const getData = (genreid) => ( dispatch, getState ) => {

    dispatch({ type: REQ_GENRES_FILMS });

    axios.get('/discover/movie', {
        params: { 
            with_genres: genreid
        }
    })
        .then( res => dispatch({
                type: RES_GENRES_FILMS,
                payload: res.data
            }))   
}
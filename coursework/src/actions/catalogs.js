import axios from '../helpers/axios';

export const REQ_CATALOG_FILMS = "REQ_CATALOG_FILMS";
export const RES_CATALOG_FILMS = "RES_CATALOG_FILMS";
export const ERROR_CATALOG_FILMS = "ERROR_CATALOG_FILMS";



export const getData = () => ( dispatch, getState ) => {

    dispatch({ type: REQ_CATALOG_FILMS });

    axios.get('/genre/movie/list')
         .then( res => dispatch({
                type: RES_CATALOG_FILMS,
                payload: res.data
            }))   
}
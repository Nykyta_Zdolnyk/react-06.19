import axios from '../helpers/axios';

export const REQ_FIND_FILMS = "REQ_FIND_FILMS";
export const RES_FIND_FILMS = "RES_FIND_FILMS";
export const ERROR_FIND_FILMS = "ERROR_FIND_FILMS";



export const getData = (query) => ( dispatch, getState ) => {

    dispatch({ type: REQ_FIND_FILMS });

    axios.get('/search/movie', {
        params: { 
            query: query
        }
    })
        .then( res => dispatch({
                type: RES_FIND_FILMS,
                payload: res.data
            }))   
}
import axios from '../helpers/axios';

export const REQ_LATEST_FILMS = "REQ_LATEST_FILMS";
export const RES_LATEST_FILMS = "RES_LATEST_FILMS";
export const ERROR_LATEST_FILMS = "ERROR_LATEST_FILMS";



export const getData = () => ( dispatch, getState ) => {

    dispatch({ type: REQ_LATEST_FILMS });

    axios.get('/movie/latest')
         .then( res => dispatch({
                type: RES_LATEST_FILMS,
                payload: res.data
            }))   
}
import axios from '../helpers/axios';

export const REQ_POPULAR_FILMS = "REQ_POPULAR_FILMS";
export const RES_POPULAR_FILMS = "RES_POPULAR_FILMS";
export const ERROR_POPULAR_FILMS = "ERROR_POPULAR_FILMS";



export const getData = () => ( dispatch, getState ) => {

    dispatch({ type: REQ_POPULAR_FILMS });

    axios.get('/movie/popular', {params:{test: 'test'}})
        .then( res => dispatch({
                type: RES_POPULAR_FILMS,
                payload: res.data
            }))   
}
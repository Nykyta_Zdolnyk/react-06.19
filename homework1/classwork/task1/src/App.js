import React, { Component } from "react";
import "./App.css";
import guests from "./guests.json";
import ListItem from "./list-item";

console.log(guests);
class App extends Component {

  state = {
    filteredGuests: []
  }

  filterGuest = (e) => {
    let query = e.target.value;
    let filterGuest = guests.filter( guest => {
      return guest.name.indexOf(query) != -1
    })
    
    this.setState({
      filteredGuests: filterGuest
    })
  }
  render() {
    const { filterGuest } = this;
    const { filteredGuests } = this.state;
    return (
      <div className="App">
        <input type="text" onChange={ filterGuest }></input>
        <ul>
          {
            !filteredGuests.length ? (
              guests.map(item => {
                return <ListItem key={item.index} guest={item} />;
              })
            ) : 
            (
              filteredGuests.map(item => {
                return <ListItem key={item.index} guest={item} />;
              })
            )
          }
        </ul>
      </div>
    );
  }
 
}

export default App;

import React, { Component } from "react";

class ListItem extends Component {
    state = {
        isArrived: false
    }

    changeHandler = (e) => {
        this.setState({
            isArrived: !this.state.isArrived
        })
    }

  render() {
    const { guest } = this.props;
    const { changeHandler } = this;
    const { isArrived } = this.state;

    return (
      <li className="list-item"> 
            <div className={isArrived ? "arrived" : ""}> {guest.name} </div>
            <button onClick={changeHandler}> Check </button>
      </li>
    );
  }
}

export default ListItem;

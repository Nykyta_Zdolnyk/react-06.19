import React from "react";


const Button = ( props ) => {
  const { changeHandler, style, id } = props;
  
  return(
    <>
        <input 
          type = "button"
          style = {style} 
          onClick = { changeHandler } 
          value = 'Click'
          index = {id} 
        />
    </>

  );
}

export default Button;

import React, { Component } from "react";

import Button from "./Button";
import Item from "./list-tem";

//http://www.json-generator.com/api/json/get/bTTuRdEVpe?indent=2

class List extends Component {
  state = {
    data: []
  };

  componentDidMount = () => {
    let url = "http://www.json-generator.com/api/json/get/bTTuRdEVpe?indent=2";

    fetch(url)
      .then(res => res.json())
      .then(res => {
        let data = res.map(user => {
          return {
            interviewed: false,
            user
          };
        });

        console.log(data);
        this.setState({ data });
      });
  };
  changeHandler = event => {
    const { data } = this.state;
    let index = event.target.attributes.index.value;
    data[index].interviewed = !data[index].interviewed;

    this.setState({
      data: data
    });
  };
  render() {
    const { data } = this.state;
    const { changeHandler } = this;

    return (
      <>
        <ul>
          {
              data.length != 0 ? (
                data.map(item => {
                    const { user, interviewed } = item;
    
                    return (
                        <Item key={user.index} user={user} interviewed={interviewed}>
                            <Button changeHandler={changeHandler} id={user.index} />
                        </Item>
                    );
                })
              ) : (
                  "Загрузка"
              ) 
                
          }
        </ul>
      </>
    );
  }
}

export default List;

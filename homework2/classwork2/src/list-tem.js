import React from "react";


const Item = ( props ) => {

    const { user, children, interviewed } = props;
    return(
        <li className={interviewed ? "checked" : ""}>
            {user.name}
            {children}
        </li>

    );
}

export default Item;

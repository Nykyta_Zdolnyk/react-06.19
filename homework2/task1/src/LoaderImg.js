import React, { Component } from 'react'


class LoaderImg extends Component {

    state = {
        loaded: false
    }
    componentDidMount() {
        const { src } = this.props

        let img = document.createElement('img');
        img.onload = this.onload;
        img.src = src;
        

        console.log(img);
    }

    onload = () => {
        
        this.setState({
            loaded: true,
        })
    }

    render() {

        const { loaded } = this.state;
        
        return(
            <div>
                {
                  loaded ? 
                    <img src={this.props.src} /> : "Loading"   
                }
            </div>
        )
    }
}


export default LoaderImg;
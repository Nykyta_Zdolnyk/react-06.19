import React from "react";


const Button = ( props ) => {
  const { changeHandler, style } = props;
  
  return(
    <>
        <input 
          type = "button"
          style = {style} 
          onClick = { changeHandler } 
          value = 'Click' 
        />
    </>

  );
}

export default Button;

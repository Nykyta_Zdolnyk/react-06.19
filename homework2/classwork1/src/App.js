import React, { Component } from "react";
import "./App.css";

import Button from './Button'

const style = {
  color: "red"
}

class App extends Component {

  changeHandler = (e) => {
    
    console.log('Click');
  }
  render() {

    const { changeHandler } = this; 
    return (
      <div className="App">
        <Button changeHandler={changeHandler} style={style}/>
      </div>
    );
  }
 
}

export default App;

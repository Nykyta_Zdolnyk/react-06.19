import React, { Component } from "react";
import "./App.css";

import Row from "./Row";
import Cell from "./Cell";

class App extends Component {
  render() {
    return (
      <div className="App">
        <table>
         
            <Row head="true">
              <Cell type="" background="red">1</Cell>
              <Cell type="date">2</Cell>
              <Cell type="number">3</Cell>
              <Cell type="money" currency="$">4</Cell>
            </Row>

            <Row>
              <Cell type="" background="red">1</Cell>
              <Cell type="date">2</Cell>
              <Cell type="number">3</Cell>
              <Cell type="money" currency="$">4</Cell>
            </Row>
         
         
        </table>
      </div>
    );
  }
}

export default App;

import React from "react";

const Cell = (props) => {
const { children, cells, type, color, background, currency } = props


  return (
    <td 
      className='td' 
      colSpan= { cells }
      style={
        {
          color: color,
          background: background,
          fontStyle: type === "date" ? "italic" : null,
          textAlign: type === "text" ? "left" : type === "number" || type === "money" ? "right" : null 
        }
      }
      > 
        { children }
        {
          type === "money" ? currency !== undefined ? currency: console.log("Error currency") : null 
        }
         
    </td>
  );
};



Cell.defaultProps = {
    type: 'text',
    cells: 1,
    background: "transparent",
    color: "black"
}

export default Cell;

import React from "react";

const Row = props => {
  const { head, children } = props;

  return head ? (
    
      <caption>{children}</caption>

  ) : (
    <tr>{children}</tr>
  );
};

Row.defaultProps = {
  head: false
};

export default Row;

import React from 'react';
import { Route, Switch } from 'react-router-dom'

import './App.css';

import Main from './components/Main';
import NotFound from './components/NotFound';
import Done from './components/Done';
import Navigation from './components/nav';
import Nodone from './components/Nodone';


function App() {
  return (
      <>
        <Navigation />
        <Switch>
          <Route exact path='/' component={Main}/>
          <Route  path='/done' component={Done}/>
          <Route exact path='/nodone' component={Nodone}/>
          <Route component={NotFound}/>
        </Switch>
    </>
    
  );
}

export default App;

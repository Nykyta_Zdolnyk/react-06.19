import React from 'react';

import Form from './Form';
import TodoList from './TodoList';

const Main = () => {

    return(
        <>
            <h1> TodoList </h1>
            <Form />
            <TodoList />
        </>
    )
}

export default Main;


import React from 'react';
import { NavLink } from 'react-router-dom'
 
const Navigation = () => {

    return(
        <>
            <NavLink exact to='/'> Main </NavLink>
            <NavLink exact to='/done'> Done </NavLink>
            <NavLink exact to='/nodone'> No Done </NavLink>
        </>
    )
}

export default Navigation;
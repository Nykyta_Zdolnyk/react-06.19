import React from 'react';

import { connect } from 'react-redux';

const Nodone = ({ nodoneTodo }) => {
   
    return(
        <>
            <h1> No done </h1>
            <ul>
                {
                    nodoneTodo.map(item => <li key={item.id}> {item.name} </li>)
                }
            </ul>
        </>
    )
}



/*
    Redux
*/

const mapStateToProps = (state) => ({

    nodoneTodo: state.todoList.filter(item => {
        return item.isdone === false
    })
})



export default connect(mapStateToProps, null)(Nodone);
import React from 'react';

import { connect } from 'react-redux';

const Done = ({ doneTodo }) => {
    return(
        <>
            <h1>Done</h1>
            <ul>
                {
                    doneTodo.map(item => <li key={item.id}> {item.name} </li>)
                }
            </ul>
        </>
    )
}



/*
    Redux
*/

const mapStateToProps = (state) => ({

    doneTodo: state.todoList.filter(item => {
        return item.isdone === true
    })
})



export default connect(mapStateToProps, null)(Done);
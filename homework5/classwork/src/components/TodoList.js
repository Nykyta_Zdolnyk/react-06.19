import React from 'react';
import { connect } from 'react-redux';

import TodoItem from './todoItem';

const TodoList = ({ todoList, check }) => {

    return(
        <div>
            <ul>
                {
                    todoList.map(item => {

                        return(
                            <TodoItem
                                key={item.id}
                                todo={item}
                                changeHandler={check(item.id)}
                            />
                        )
                    })
                }
            </ul>
        </div>
    )
}

/* 
    Redux
*/

const mapStateToProps = ( state ) => ({
    todoList: state.todoList

});

const mapDispatchToProps = ( dispatch ) => ({

    check: (id) => (e)=> {
        dispatch({ type: 'CHECK', payload: id })
    }
    
})

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
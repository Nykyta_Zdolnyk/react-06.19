import React from 'react';

const TodoItem = ({ todo, changeHandler }) => {

    return(
        <li>
            {todo.name}
            <input 
                type='checkbox' 
                onChange={changeHandler}
                defaultChecked={todo.isdone}
            />
        </li>
    )
}

export default TodoItem;
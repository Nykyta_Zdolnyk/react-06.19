import React from 'react';
import { connect } from 'react-redux';

const Form = ({ addTodo }) => {

    return(
        <>
            <input type='text' id='text'></input>
            <button onClick={addTodo}> Add </button>
        </>
    )
}

/*
    Redux
*/

const mapStateToProps = (state) => {

}

const mapDispatchToProps = (disptach) => ({
    addTodo: (e) => {
        let input = document.getElementById('text');

        disptach({ type: "ADD_TODO", payload: input.value });
        
        input.value = '';

    }
})



export default connect(null, mapDispatchToProps)(Form);
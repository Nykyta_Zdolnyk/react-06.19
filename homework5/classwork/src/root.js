import React from 'react'
import { 
    BrowserRouter,
    Route
} from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './redux/store';

import App from './App';


const Root = () => {
    return (
        <>
            <Provider store={store}>
                <BrowserRouter >
                    <Route component={App} />
                </BrowserRouter>
            </Provider>
            
        </>
    )
}

export default Root;

let stateData = {
    todoCount: 0,
    todoList: []
}

const reducers = (state = stateData, action) => {

    switch( action.type ) {
         
        case 'ADD_TODO':
            return {
                ...state,
                todoList:[
                    ...state.todoList, {
                        id: ++state.todoCount,
                        name: action.payload,
                        isdone: false
                    }
                ]
            
            }

        case 'CHECK':

                let ind;
                state.todoList.forEach( (item, index) => {
                    if (item.id === action.payload) {
                      ind = index;
                      return;
                    }
                })
                console.log("index", ind);
                let arr = state.todoList;
                arr[ind].isdone = !arr[ind].isdone;
                console.log(arr);
      
                return {
                  ...state,
                  todoList: arr
                }

        default:
            return state
    }

}

export default reducers;
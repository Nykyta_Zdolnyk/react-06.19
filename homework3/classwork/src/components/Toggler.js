import React, { Component } from 'react';
import PropTypes from 'prop-types';


class Toggler extends Component {

    render() {
        const{ children, active, togglerChange, keyName } = this.props;
        return (
            <div className="toggler__core">
                {
                    React.Children.map( children, Toggler => {
                        return React.cloneElement(
                            Toggler,
                            {
                                action: togglerChange( Toggler.props.value, keyName ),
                                active: active === Toggler.props.value ? true : false
                            }
                        );
                    })
                }
            </div>
        )
    }
}

Toggler.propTypes = {
    active: PropTypes.string,
    keyName: PropTypes.string.isRequired,
    togglerChange: PropTypes.func.isRequired,
    children: PropTypes.arrayOf(PropTypes.element)
}

export default Toggler;
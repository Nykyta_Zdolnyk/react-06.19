import React from 'react';
import PropTypes from 'prop-types';


const Input = ({ name, type, placeholder, value, handler}) => {

    return(
        <label>
            <div></div>
            <input
                type={type}
                placeholder={placeholder}
                value={value}
                onChange={handler(name)}
            />
        </label>
    )
}

Input.propTypes = {

    type: PropTypes.oneOf(['text', 'password', 'number']),
    placeholder: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.any]),
    handler: PropTypes.func
}

export default Input;
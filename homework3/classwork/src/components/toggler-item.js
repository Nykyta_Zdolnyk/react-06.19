import React from 'react'

export const TogglerItem = ({ value, active, action }) => {

    return (
        <div className={
            active ?
            "toggler__item active" :
            "toggler__item"
        }
            onClick={action}
        >
            {value}
        </div>
    )
}

export default TogglerItem;

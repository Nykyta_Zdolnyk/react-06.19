import React, { Component } from "react";
import "./App.css";

import Toggler from "./components/Toggler";
import TogglerItem from "./components/toggler-item";
import Input from "./components/Input";

// import { Toggler, TogglerItem, Input } from './components/'

class App extends Component {
  state = {
    layout: "right",
    gender: "female"
  };

  inputChange = (name) => (e) => {
    this.setState({
      [name]: e.target.value
    })
  };

  togglerChange = (value, keyName) => e => {
    this.setState({
      [keyName]: value
    });
  };

  onSubmit = () => {
    console.log(this.state)
  }

  render() {
    const { layout, gender } = this.state;
    const { togglerChange, inputChange, onSubmit } = this;

    return (
      <div className="App">
        <form onSubmit={onSubmit}>

        <Input
            name={'name'}
            placeholder={"Enter your name"}
            type={"text"}
            handler={inputChange}
          />

          <Input
            name={'password'}
            placeholder={"Enter password"}
            type={"password"}
            handler={inputChange}
          />

          <Toggler
              keyName="gender"
              togglerChange={togglerChange}
              active={gender} 
          >
            <TogglerItem value="male" />
            <TogglerItem value="female" />
          </Toggler>

          <Input
            name={'age'}
            placeholder={"Enter your age"}
            type={"number"}
            handler={inputChange}
          />

          <Toggler
            keyName="layout"
            togglerChange={togglerChange}
            active={layout}
          >
            <TogglerItem value="left" />
            <TogglerItem value="center" />
            <TogglerItem value="right" />
            <TogglerItem value="baseline" />
          </Toggler>

          <Input
            name={'language'}
            placeholder={"Enter language"}
            type={"text"}
            handler={inputChange}
          />

          <br/>

          <input type='submit' ></input>
        </form>
      </div>
    );
  }
}

export default App;

import React, { Component } from "react";

class Loader extends Component {
  state = {
    img: ""
  };

  onChange = e => {
    let files = e.target.files;
    console.log(files);
    let reader = new FileReader();

    reader.readAsDataURL(files[0]);
    reader.onload = e => {
      this.setState({
        img: e.target.result
      });
    };
  };
  render() {
    let { onChange } = this;
    let { img } = this.state;
    return (
      <div>
        Preload image)
      <label>
        <div 
          className="preload"
          style={img ? { backgroundImage: "url(" + img + ")" } : null}
        />
        <input className="file" type="file" onChange={onChange} hidden={true}/>
      </label>
      </div>
    );
  }
}

export default Loader;

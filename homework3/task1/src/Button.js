import React, { Component } from 'react';

class Button extends Component {

    inputRef = React.createRef();

    componentDidMount = () => {
        console.log('myRef = ', this.inputRef)
    }

    handler = (e) => {
        this.inputRef.current.className += ' animation';
    }

    render() {
        let { handler } = this;
        let { inputRef } = this; 
        return(
            <div>
                <input 
                    type='button' 
                    className='button' 
                    ref={inputRef} 
                    onClick={handler} 
                    value='Click me' 
                />
            </div>
        )
    }
}

export default Button;
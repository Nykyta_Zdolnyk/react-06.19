import React, { Component } from "react";

class Song extends Component {

    state = {
        songs: [],
        loaded: false
    }

    componentDidMount() {

        fetch('https://jsonplaceholder.typicode.com/photos')
            .then( res => res.json() )
            .then( res => {
                
                const { songid } = this.props.match.params
                let filtered = res.filter( item => item.albumId == songid);

                this.setState({ songs: filtered, loaded:true })
            })  
    }

    render() {

        const { songs, loaded } = this.state;

        return(
            <>
                {
                    loaded ? (
                        <ul>
                            {
                                songs.map( item => (
                                    <li key={item.id}> 
                                        <div>
                                            <div> {item.title} </div>
                                            <div><img src={item.thumbnailUrl}></img></div>
                                        </div> 
                                    </li>))
                            }
                        </ul>
                    ) : 
                    ("Loading...")
                }
            </>
        )
    }
}
export default Song;
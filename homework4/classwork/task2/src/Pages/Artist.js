import React, { Component } from "react";
import { Link } from "react-router-dom";

class Artist extends Component {
 
    state  = {
            artist: [],
            loaded: false       
    }
    
    componentDidMount () {
    
        fetch('https://jsonplaceholder.typicode.com/users')
            .then( res => res.json() )
            .then( res => this.setState({ artist: res, loaded: true }) )
    }

    render() {

        const { artist, loaded } = this.state;
        return(
            <>
               {
                   loaded ? (
                       
                        artist.map( item => {
                            const { id, name } = item;
                            return (
                                <div key={id}>
                                    <Link to={`/album/${id}`}> 
                                        {name} 
                                    </Link>
                                </div>
                            )
                        })
                   ) : 
                   ('Loading...')
               }
            </>
        )
    }        
} 

export default Artist;
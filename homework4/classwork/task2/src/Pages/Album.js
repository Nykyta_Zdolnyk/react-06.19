import React, { Component } from "react";
import { Link } from "react-router-dom";


class Album extends Component {
  state = {
    albums: [],
    loaded: false
  };

  
  componentDidMount = () => {

    fetch('https://jsonplaceholder.typicode.com/albums/')
      .then( res => res.json() )
      .then( res => {

          const { albumid } = this.props.match.params;

          let filtered = res.filter( item => item.userId == albumid);

          this.setState({ albums: filtered, loaded: true });
      } )
  }

  render() {
    
    const { albums, loaded } = this.state;
    console.log(this.props)

    return (
        <>
          {
              loaded ? (
                  
                  albums.map( album => {

                    const {pathname} = this.props.location

                    return (
                      <div key={album.id}>
                        <Link 
                          to={`${pathname}/songs/${album.id}`}
                          >
                             {album.title} 
                          </Link>
                      </div>
                    )
                  })
              ) : 
              ("Loading...")
          }
        </>
    );
  }
}

export default Album;

import React, { Component } from "react";
import "./App.css";

import { Route, Switch, Link } from "react-router-dom";

import Artist from "./Pages/Artist";
import Album from "./Pages/Album";
import Song from "./Pages/Song";
import NotFound from "./Pages/NotFound";

class App extends Component {

 
  render() {

    return (
      <div className="App">
        <Switch>
          <Route exact path="/" component={Artist} />
          <Route exact path="/album/:albumid" component={Album} />
          <Route exact path="/album/:albumid/songs/:songid" component={Song} />
          <Route component={NotFound} />
        </Switch>

        
      </div>
    );
  }
}

export default App;

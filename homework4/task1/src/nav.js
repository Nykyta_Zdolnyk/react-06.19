import React from 'react'
import { NavLink } from 'react-router-dom'

const Navigation = () => {

    return(
        <div>
            <NavLink exact to='/'> Главаная </NavLink>
            <NavLink exact to='/posts/limit/13'> Последение новости </NavLink>

        </div>
    )
}

export default Navigation;
import React, { Component } from 'react';

class LastNews extends Component {

    state = {
        posts: [],
        loaded: false,
    }
    
    componentDidMount() {

        fetch(`https://jsonplaceholder.typicode.com/posts`)
            .then( res => res.json() )
            .then( res => this.setState({ posts: res, loaded: true }));
    }

    render() {

        const { limit } = this.props.match.params;
        const { posts, loaded } = this.state;
        return(
            <>
                {
                    loaded ? 
                    (
                        <ol>
                        {
                            posts
                                .slice(0, limit)
                                .map( post => {
    
                                    return (
                                        <li key={post.id}>      
                                                <div> <h4>{post.title}</h4> </div>
                                        </li>
                                    )
                                })
                        }
                    </ol>
                    ) : 
                    (
                        'Загрузка постов'
                    )
                }  
            </>
        )
    }
}

export default LastNews
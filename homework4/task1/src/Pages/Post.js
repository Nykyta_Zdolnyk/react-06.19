import React, { Component } from 'react';

class Post extends Component {

    state = {
        post: null,
        comments: [],
        loadedPost: false,
        loadedComments: false,
        loadingComments: false
    }

    changeHandler = () => {

        const { postid } = this.props.match.params;

        this.setState({ loadingComments: true });

        fetch(`https://jsonplaceholder.typicode.com/posts/${postid}/comments`)
            .then( res => res.json() )
            .then( res => this.setState({ comments: res, loadedComments: true }) )
    } 

    componentDidMount() {

        const { postid } = this.props.match.params;
        const { loadedPost } = this.state;
        

        fetch(`https://jsonplaceholder.typicode.com/posts/${postid}`)
            .then( res => res.json() )
            .then( res => this.setState({ post: res, loadedPost: true }) )
    }


    render() {

        const { loadedPost, post, loadedComments, comments, loadingComments } = this.state;
        const { changeHandler } = this;

        return(
            <>
                {
                    loadedPost ? 
                    (
                        <>
                            <h2> {post.title} </h2>
                            <div> {post.body} </div>
                            <input type='button' value='Показать комментарии' onClick={changeHandler}/>

                            {
                                loadingComments ? 
                                (
                                    loadedComments ? (
                                        <ul>
                                            {
                                                comments.map( item => (
                                                    <li key={item.id}>
                                                        {
                                                            <div>
                                                                <div>
                                                                    <b>Name:</b> {item.name}, <b>Email:</b> {item.email}
                                                                </div>
                                                                <div><b>comment:</b> {item.body} </div>
                                                            </div>
                                                        }
                                                    </li>
                                                ) ) 
                                            }
                                        </ul>
                                        
                                    ) : 
                                    ('Загрузка комментов')
                                ) : 
                                ('')
                            }
                        </>
                    ) :
                    (
                        'Загрузка поста ...'
                    )
                }
            </>
        )
    }
}

export default Post;
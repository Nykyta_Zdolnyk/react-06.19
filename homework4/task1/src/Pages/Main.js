import React, { Component } from 'react';
import { Link } from 'react-router-dom'
class Main extends Component {

    state = {
        posts: [],
        loaded: false,
        limit: 20
    }

    changeHandler = () => {
        
        const { limit } = this.state;
        this.setState({ limit: limit + 20 })
    }

    componentDidMount() {

        fetch('https://jsonplaceholder.typicode.com/posts')
            .then( res => res.json() )
            .then( res => this.setState({ posts: res, loaded: true }) ) 
    }

    render() {

        const { posts, limit, loaded } = this.state;
        const { changeHandler } = this

        return(
            <>
                {
                    loaded ? (
                        <>
                            <ol>
                                {
                                    posts
                                        .slice(0, limit)
                                        .map( post => {
            
                                            return (
                                                <li key={post.id}>      
                                                        <div> 
                                                            <Link 
                                                                    to={`/posts/${post.id}`}
                                                            >
                                                                {post.title}
                                                            </Link> 
                                                        </div>
                                                </li>
                                            )
                                        })
                                }
                            </ol>
        
                            <input type='button' value='Показать еще' onClick={changeHandler}/>
                        </>
                    ) : 
                    (
                        'Загрузка новостей ...'
                    )
                }
               
            </>
        )
    }
}

export default Main
import React from 'react';
import { Switch, Route } from 'react-router-dom'

import './App.css';

import Main from './Pages/Main';
import NotFound from './Pages/NotFound';
import LastNews from './Pages/LastNews';
import Navigation from './nav';
import Post from "./Pages/Post"

function App() {
  return (
    <div className="App">
      
      <Navigation />

      <Switch>
        <Route exact path='/' component={Main} />
        <Route exact path='/posts/limit/:limit' component={LastNews} />
        <Route exact path='/posts/:postid'  render={(props) => <Post  {...props} />}/>
        <Route component={NotFound} />
      </Switch>
    
    </div>
  );
}

export default App;
